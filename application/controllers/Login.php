<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Login_model');
    }

    public function index()
    {
        // load view admin/overview.php
        $this->load->view("pages/page_login");
    }

    function login_access(){
        $email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $response =$this->Login_model->login_check($email,$password);

		if(count($response) > 0){
			$loginData = array(
				"id" => $response[0]['id'],
				"email" => $response[0]['email'],
				"role" => $response[0]['role'],
				"status" => "login"
			);
			$this->session->set_userdata($loginData);
			redirect('Input');
		}else{
			echo "Login Failed !";
		}


    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url();
        redirect($url);
    }
}
