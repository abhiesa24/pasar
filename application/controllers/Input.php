<?php

class Input extends CI_Controller {

    function __construct(){
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("Login"));
        }
        $this->load->model('Input_model');
    }

    public function index()
    {
        // load view admin/overview.php
		$data['list_produk'] = $this->Input_model->getAllProduk();
        $this->load->view("pages/page_input", $data);

    }

    public function addData(){

    	$code = $_POST['code'];
    	$qty = $_POST['qty'];
    	$price = $_POST['price'];
    	$total = $_POST['total'];
    	$user = $this->session->userdata('id');

    	$data = array(
    		'goods' => $code,
			'user' => $user,
			'quantity' => $qty,
			'price' => $price,
			'total' => $total
		);

    	$result = $this->Input_model->addData($data);

    	if($result > 0){
    		echo "Ok";
		}else{
    		echo "Failed";
		}

	}

}
