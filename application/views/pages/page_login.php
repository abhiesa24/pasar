<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pasar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="<?php echo base_url('fonts/material-design-iconic-font/css/material-design-iconic-font.css') ?>" />
    <!-- STYLE CSS -->
    <link rel="stylesheet" href="<?php echo base_url('css/style.css') ?>" />
</head>
<body>
<form action="<?php echo base_url('login/login_access'); ?>" method="post">
<div class="wrapper">
    <div id="wizard">
        <h1>Login</h1>
        <br>
        <section>
            <div class="form-row">
                <div class="form-row form-group">
                    <div class="form-row form-group">
                        <div class="form-holder">
                            <input name="email" id="idUserName" type="text" class="form-control" placeholder="Username...">
                        </div>
                    </div>
                    <div class="form-row form-group">
                        <div class="form-holder">
                            <input name="password" id="idUserPassword" type="password" class="form-control" placeholder="Password...">
                        </div>
                    </div>
                    <button
                             style="width: 30%; height: 42px" type="submit">
                        Login
                    </button>
                </div>
            </div>
        </section>


    <div class="image-holder">
        <img src="images/form-wizard.png" alt="" >
    </div>

    </div>


</div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    //function login() {
    //    window.location.href = '<?php //echo base_url() ?>//Login';
    //    // alert(total);
    //
    //}

    //function addInputToDatabase() {
    //    $.ajax({
    //        url: "<?php //echo site_url('Input/addInput'); ?>//",
    //        type: "post",
    //        data: {
    //            // name: name,
    //            // billed_to: billed_to,
    //            // shipp_to: shipp_to,
    //            // customer: customer,
    //            // headquarter: headquarter,
    //            // due_date: due_date,
    //        },
    //        cache: false,
    //        success: function (response) {
    //            // alert("gg");
    //            alert("response :" + response);
    //            if (response == "    200") {
    //                swal("Add Project Success!", "Thank you!", "success");
    //                closeRegisterProject();
    //                getListProject();
    //                $('#registerProject').hide();
    //                $('#divData').show();
    //                buttonsProject.innerHTML = "Add Project";
    //                document.getElementById('btnProject').disabled = false;
    //
    //            } else {
    //                swal("Add Project Failed!", "Sorry!", "error");
    //                buttonsProject.innerHTML = "Add Project";
    //                document.getElementById('btnProject').disabled = false;
    //
    //                // closeRegisterProject();
    //            }
    //        }
    //    });
    //}

</script>

<script src="<?php echo base_url('js/jquery-3.3.1.min.js') ?>"></script>

<!-- JQUERY STEP -->
<script src="<?php echo base_url('js/jquery.steps.js') ?>"></script>

<script src="<?php echo base_url('js/main.js') ?>"></script>
<!-- Template created and distributed by Colorlib -->
</body>
</html>
