<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pasar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="<?php echo base_url('fonts/material-design-iconic-font/css/material-design-iconic-font.css') ?>" />
    <!-- STYLE CSS -->
    <link rel="stylesheet" href="<?php echo base_url('css/style.css') ?>" />
</head>
<body>
<div class="wrapper">
    <div class="image-holder">
        <img src="images/form-wizard.png" alt="" >
    </div>

        <div id="wizard">

            <!-- SECTION 1 -->
            <h1>Form Input Pasar</h1>
            <br>
            <section>
                <div class="form-row">
                    <label for="">
                        Choose Product *
                    </label>
                    <div class="form-holder">
                        <select name="productName" id="inputProduct" class="form-control">
							<?php foreach ($list_produk as $row): ?>
								<option value="<?= $row['code'] ?>"><?= $row['name'] ?></option>
							<?php endforeach; ?>
                        </select>
                        <i class="zmdi zmdi-caret-down"></i>
                    </div>
                </div>
                <div class="form-row form-group">
                    <div class="form-holder">
                        <label for="">
                            Weight *
                        </label>
                        <input name="idProductQuantity" id="idProductQuantity" type="text" class="form-control" style="width: 85%" value="0"><small style="margin:5px">  gram</small>
                    </div>
                    <div>
                    <button  id="btnAdd1kg"
                             style="width: 32%; height: 42px">
                        1 KG
                    </button>
                    <button  id="btnAdd2kg"
                             style="width: 33%; height: 42px">
                        2 KG
                    </button>
                    <button  id="btnAdd3kg"
                             style="width: 32%; height: 42px">
                        3 KG
                    </button>
                    </div>
                    <div>
                    <button  id="btnAdd5kg"
                             style="width: 32%; height: 42px; margin-top: 4px">
                        5 KG
                    </button>
                    <button  id="btnAdd10kg"
                             style="width: 33%; height: 42px">
                        10 KG
                    </button>
                    <button  id="btnAdd20kg"
                             style="width: 32%; height: 42px">
                        20 KG
                    </button>
                    </div>

                </div>

                <div class="form-row form-group">
                    <div class="form-holder">
                        <label for="">
                            Price *
                        </label>
                        <input name="productPrice" id="idProductPrice" type="text" class="form-control" onchange="changeTotal()" value="0">
                    </div>
                </div>
                <div class="form-row form-group">
                    <div class="form-holder">
                        <label for="">
                            Total
                        </label>
                        <input name="productTotal" id="idProductTotal" type="text" class="form-control" value="0" disabled>
                    </div>
                </div>

            </section><br>
            <button  onclick="addData()"
                    style="width: 100%; height: 42px">
                Submit
            </button>

        </div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    changeTotal();
    $(document).ready(function(){
        $("#btnAdd1kg").click(function(){
            $("#idProductQuantity").val("1000");
        });
        $("#btnAdd2kg").click(function(){
            $("#idProductQuantity").val("2000");
        });
        $("#btnAdd3kg").click(function(){
            $("#idProductQuantity").val("3000");
        });
        $("#btnAdd5kg").click(function(){
            $("#idProductQuantity").val("5000");
        });
        $("#btnAdd10kg").click(function(){
            $("#idProductQuantity").val("10000");
        });
        $("#btnAdd20kg").click(function(){
            $("#idProductQuantity").val("20000");
        });
    });

    function changeTotal() {
        var price = document.getElementById("idProductPrice").value;
        var qty = document.getElementById("idProductQuantity").value;
        var total = (price * qty) / 1000;
        $("#idProductTotal").val(total);
        // alert(total);

    }


</script>

<script src="<?php echo base_url('js/jquery-3.3.1.min.js') ?>"></script>

<!-- JQUERY STEP -->
<script src="<?php echo base_url('js/jquery.steps.js') ?>"></script>

<script src="<?php echo base_url('js/main.js') ?>"></script>

<!-- Template created and distributed by Colorlib -->
</body>
</html>

<script>
	function addData() {
		var code = document.getElementById("inputProduct").value;
		var qty = document.getElementById("idProductQuantity").value;
		var price = document.getElementById("idProductPrice").value;
		var total = document.getElementById("idProductTotal").value;
		//var type = document.getElementById("type").value;

		//alert(idC);
		$.ajax({
			url: "<?php echo base_url('Input/addData'); ?>",
			type: "post",
			data: {
				code:code,
				qty:qty,
				price:price,
				total:total,
			},
			cache: false,
			success: function (response) {
				// alert(response);
				if(response == "Ok"){
					alert('Sukses');
					document.getElementById("inputProduct").value = "";
					document.getElementById("idProductQuantity").value = "";
					document.getElementById("idProductPrice").value = "";
					document.getElementById("idProductTotal").value = "";
				}else if(response == "Failed"){
					alert('Failed !');
				}
			}
		});
	}
</script>
