<?php
/**
 * Created by PhpStorm.
 * User: Ajie Putra
 * Date: 2/19/2019
 * Time: 6:27 PM
 */
class Input_model extends CI_Model{
    function getInput(){
        return $this->db->get('market_transactions');
    }

    public function getAllProduk(){
		return $this->db->get('goods')->result_array();
	}

	public function addData($data){
		$this->db->insert('market_transactions',$data);
		return $this->db->affected_rows();
	}
}
